using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsaacMove : MonoBehaviour
{
    
    public float playerSpeed;
    public float playerHealth;
    public float playerLife;
    public float playerTearRate;
    public float playerDmg;
    public float playerRange;
    public float shootSpeed;

    /**********
        Player Coponents 
     ***********/

    Rigidbody2D playerRb;
    private bool canShoot;
    private float nextFire = 0;

    /***********
     GameObjects
    ***********/
    public GameObject tears;
    public GameObject head;
    public GameObject rightEye;
    public GameObject leftEye;
    private Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        playerRb = GetComponent<Rigidbody2D>();
        // controller = gameObject.AddComponent<CharacterController>();

        
        anim = head.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
      
        //Player Movement
        playerRb.velocity = new Vector2(Input.GetAxis("Horizontal") * playerSpeed, Input.GetAxis("Vertical") * playerSpeed);

        // Check if player is shooting 
        canShoot = (Input.GetAxis("TearVertical") != 0 || Input.GetAxis("TearHorizontal") != 0) ? true : false;

        CheckDirectionHead();
        CheckDirectionBody();

    }


    // Player Aiming
    private void CheckDirectionHead()
    {
        string vDirection = "";
        string hDirection = "";
        switch (Input.GetAxis("TearVertical"))
        {
            case 0:
                anim.SetBool("ShootingDown", false);
                vDirection = "";
                //Debug.Log("Iddle");
                break;
            case >0:
                vDirection = "Up";
                //Debug.Log("Up");
                break;
            case <0:
                vDirection = "Down";
                //Debug.Log("Down");
                break;
        }
        switch (Input.GetAxis("TearHorizontal"))
        {
            case 0:
                hDirection = "";
                //Debug.Log("Iddle");
                break;
            case >0:
                hDirection = "Right";
                //Debug.Log("Right");
                break;
            case <0:
                hDirection = "Left";
                //Debug.Log("Left");
                break;
        }
        ShootingTear(vDirection, hDirection);


    }
    // Player DirectionMoving
    private void CheckDirectionBody()
    {
        switch (Input.GetAxis("Vertical"))
        {
            case 0:
                //Debug.Log("Iddle");
                break;
            case 1:
                //Debug.Log("Up");
                break;
            case -1:  
                //Debug.Log("Down");
                break;
        }
        switch (Input.GetAxis("Horizontal"))
        {
            case 0:
                //Debug.Log("Iddle");
                break;
            case 1:
                ShootingTear("Right");
                //Debug.Log("Right");
                break;
            case -1:
                ShootingTear("Left");
                //Debug.Log("Left");
                break;
        }

    }

    // Function To shooting Tears
    private void ShootingTear(string Vdirection = "", string Hdirection = "") 
    {
        Vector3 position = new Vector3();
        Vector2 forward = new Vector2();
        
        

        switch (Vdirection)
        {
            /*default:
                forward = new Vector2();
                break;*/
            case "Up":
                
                forward.y = 1;
                position = rightEye.transform.position;
                break;
            case "Down":
                
                forward.y = -1;
                position = rightEye.transform.position;
                break;
        }
        switch (Hdirection)
        { 
            case "Right":
                forward.x = 1;
                position = rightEye.transform.position;
                break;
            case "Left":
                forward.x = -1;
                position = rightEye.transform.position;
                break;
        }

        if (canShoot && Time.time > nextFire)
        {
            anim.SetBool("ShootingDown", true);
           /* SpriteRenderer spriteHead = head.GetComponent<SpriteRenderer>();
            spriteHead.SetSprite()*/
            nextFire = Time.time + playerTearRate; 
            GameObject Tears = Instantiate(tears, position, Quaternion.identity);   //Instantiate Tears
            Tears.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(forward.x * shootSpeed, forward.y * shootSpeed); // Give Velocity to tears
            Destroy(Tears.gameObject,playerRange);          //Destroy object -> playerRange Use as a timer
            //GetComponent<AudioSource>().Play();
        }
        else
        {
            anim.SetBool("ShootingDown", false);
        }
        
    }

}        